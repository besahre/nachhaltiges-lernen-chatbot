# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions



from numbers import Number
from tracemalloc import start
from typing import Any, Text, Dict, List
from datetime import date, datetime, timedelta
from xml import dom
import requests
from sqlalchemy import DateTime, Float, false
import json

from rasa_sdk import Action, Tracker
from rasa_sdk.events import SlotSet, AllSlotsReset, FollowupAction, ReminderScheduled
from rasa_sdk.executor import CollectingDispatcher

# Liest die Config ein, damit die Buttons die korrekte URL bekommen und wir wissen, wo sich Mattermost befindet
# Als Action registriert damit es am Anfang initialisiert wird
class SetUpConfigFile(Action):
    def name(self) -> Text:
        with open('editable_config.json') as data_file:
            data_loaded = json.load(data_file)
        # url um an Mattermost zu senden
        global mattermost_webhook_url
        mattermost_webhook_url = data_loaded["mattermost_webhook_url"]
        # Unsere URL damit bei Klick auf Button wir eine HTTP Anfrage erhalten
        global rasa_webhook_url
        rasa_webhook_url = data_loaded["rasa_webhook_url"]
        # Enddatum der Studie
        global end_date
        end_date = datetime(year=data_loaded["end_year"], month=data_loaded["end_month"], day=data_loaded["end_day"])
        print("Setup Mattermost_Webhook: " + str(mattermost_webhook_url))
        print("Setup Rasa Webhook:  " + str(rasa_webhook_url))
        return "read_config"

# Setzt in Rasa den Nutzernamen und setzt eine automatische Nachricht bei Ende der Studie        
class SetUsernameAction(Action):
    def name(self) -> Text:
        return "action_set_username"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        sender_id = tracker.current_state()['sender_id']
        events = []
        events.append(ReminderScheduled(
                intent_name = 'end_of_study', 
                trigger_date_time = end_date,
                name = "End of study"))

        events.append(SlotSet('username', sender_id))
        return events

# Fragt ob der Nutzer bereit zum Reflektieren ist - erlaubt Verschiebung
class AskForReadyness(Action):
    def name(self) -> Text:
        return "action_ask_for_readyness"

    def run (self, 
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        username = tracker.current_state()['sender_id']
        message = "Einen wunderschönen guten Tag, " + username + "! Hätten Sie etwas Zeit für eine kurze Reflexionssitzung?"
        attachments=self.getAttachementReminder()
        OutgoingWebhook.send_via_webhook(message=message, receiver=username, attachment=attachments)
        # Wenn der Nutzer mich ignoriert, sende ich morgen nochmal ne Anfrage
        events = []
        events.append(ReminderScheduled(
                    intent_name = 'start_reflection_session', 
                    trigger_date_time = datetime.now() + timedelta(days=1), 
                    name = "reflection_session", 
                    kill_on_user_message= True))
        return events

    def getAttachementReminder(self) -> List[Dict[Text, Any]]:
        return [
                    {
                    "actions": [
                        {
                            "name": "Ja",
                            "integration": {
                                "url": rasa_webhook_url,
                                "context": {
                                    "text": "start",
                                }
                            }
                        },
                        {
                            "name": "In 5 Minuten?",
                            "integration": {
                                "url": rasa_webhook_url,
                                "context": {
                                    "text": "/i_want_5_min"
                                }
                            }
                        }
                    ]
                    }
                ] 
# Schickt in 5 Minuten erneut eine Erinnerung
class RemindFiveMin(Action):
    def name(self) -> Text:
        return "action_remind_five_min"

    def run(self, 
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        # Ermöglicht den Nutzer, einen neuen Reminder in 5min zu setzen
        username = tracker.current_state()['sender_id']
        reminder_date = datetime.now()
        reminder_distance = timedelta(minutes=5)
        reminder_date = reminder_date + reminder_distance
        
        # Reguläre Erinnerung
        events = []
        events.append(
                ReminderScheduled(
                    intent_name = 'start_reflection_session', 
                    trigger_date_time = reminder_date, 
                    name = "reflection_session", 
                    kill_on_user_message= True))
        text = "Sie werden in 5 Minuten erneut erinnert " + reminder_date.strftime("(%H:%M Uhr)") + "."
        OutgoingWebhook.send_via_webhook(message=text, receiver=username, attachment=[] )

        return events

# Zeitpunkt der nächsten Reflexion - Wochentag
class AskWeekday(Action):
    def name(self) -> Text:
        return "action_ask_weekday"
    def run(self, 
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        username = tracker.current_state()['sender_id']
        text = "Welcher Wochentag würde Ihnen passen?"
        attachments = [
                    {
                    "actions": [
                        {
                            "name": "Montag",
                            "integration": {
                                "url": rasa_webhook_url,
                                "context": {
                                    "text": "/specify_weekday{\"weekday\" : \"Montag\"}",
                                }
                            }
                        },
                        {
                            "name": "Dienstag",
                            "integration": {
                                "url": rasa_webhook_url,
                                "context": {
                                    "text": "/specify_weekday{\"weekday\" : \"Dienstag\"}",
                                }
                            }
                        },
                        {
                            "name": "Mittwoch",
                            "integration": {
                                "url": rasa_webhook_url,
                                "context": {
                                    "text": "/specify_weekday{\"weekday\" : \"Mittwoch\"}",
                                }
                            }
                        },
                        {
                            "name": "Donnerstag",
                            "integration": {
                                "url": rasa_webhook_url,
                                "context": {
                                    "text": "/specify_weekday{\"weekday\" : \"Donnerstag\"}",
                                }
                            }
                        },
                        {
                            "name": "Freitag",
                            "integration": {
                                "url": rasa_webhook_url,
                                "context": {
                                    "text": "/specify_weekday{\"weekday\" : \"Freitag\"}",
                                }
                            }
                        },
                        {
                            "name": "Jetzt",
                            "integration": {
                                "url": rasa_webhook_url,
                                "context": {
                                    "text": "/specify_weekday{\"weekday\" : \"jetzt\"}",
                                }
                            }
                        }
                    ]
                    }
                ]
        OutgoingWebhook.send_via_webhook(message=text, receiver=username, attachment=attachments)
        # Erinnerung wenn die Antwort länger als ein Tag dauert
        return [ReminderScheduled(
            intent_name = 'choose_another_day', 
            trigger_date_time = datetime.now() + timedelta(days=1), 
            name = "reflection_session", 
            kill_on_user_message= True)]

# Zeitpunkt der nächsten Reflexion - Uhrzeit
class AskTime(Action):
    def name(self) -> Text:
        return "action_ask_time"
    def run(self, 
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        username = tracker.current_state()['sender_id']
        weekday = tracker.get_slot("weekday")
        time = tracker.get_slot("time")
        events = []
        if time == None and str(weekday) != "jetzt":
            text = "Bitte wählen Sie eine Uhrzeit am " + weekday + " :"
            OutgoingWebhook.send_via_webhook(message=text, receiver=username, attachment=self.getTimeAttachement() )
        else:
            setReminder = SetNewReminderForThisWeek()
            events = setReminder.run(dispatcher=dispatcher, tracker=tracker, domain=domain)
                    
        events.append(ReminderScheduled(
                intent_name = 'choose_another_day', 
                trigger_date_time = datetime.now() + timedelta(days=1), 
                name = "reflection_session", 
                kill_on_user_message= True))
        return events
    def getTimeAttachement(self) -> List[Dict[Text, Any]]:
        return [
                    {
                    "actions": [
                        {
                            "name": "8:00",
                            "integration": {
                                "url": rasa_webhook_url,
                                "context": {
                                    "text": "/specify_time{\"time\" : \"8:00\"}",
                                }
                            }
                        },                        
                        {
                            "name": "8:30",
                            "integration": {
                                "url": rasa_webhook_url,
                                "context": {
                                    "text": "/specify_time{\"time\" : \"8:30\"}",
                                }
                            }
                        },
                        {
                            "name": "9:00",
                            "integration": {
                                "url": rasa_webhook_url,
                                "context": {
                                    "text": "/specify_time{\"time\" : \"9:00\"}",
                                }
                            }
                        },
                        {
                            "name": "9:30",
                            "integration": {
                                "url": rasa_webhook_url,
                                "context": {
                                    "text": "/specify_time{\"time\" : \"9:30\"}",
                                }
                            }
                        },
                        {
                            "name": "10:00",
                            "integration": {
                                "url": rasa_webhook_url,
                                "context": {
                                    "text": "/specify_time{\"time\" : \"10:00\"}",
                                }
                            }
                        },
                        {
                            "name": "10:30",
                            "integration": {
                                "url": rasa_webhook_url,
                                "context": {
                                    "text": "/specify_time{\"time\" : \"10:30\"}",
                                }
                            }
                        },
                        {
                            "name": "11:00",
                            "integration": {
                                "url": rasa_webhook_url,
                                "context": {
                                    "text": "/specify_time{\"time\" : \"11:00\"}",
                                }
                            }
                        },
                        {
                            "name": "11:30",
                            "integration": {
                                "url": rasa_webhook_url,
                                "context": {
                                    "text": "/specify_time{\"time\" : \"11:30\"}",
                                }
                            }
                        },
                        {
                            "name": "12:00",
                            "integration": {
                                "url": rasa_webhook_url,
                                "context": {
                                    "text": "/specify_time{\"time\" : \"12:00\"}",
                                }
                            }
                        },
                        {
                            "name": "12:30",
                            "integration": {
                                "url": rasa_webhook_url,
                                "context": {
                                    "text": "/specify_time{\"time\" : \"12:30\"}",
                                }
                            }
                        },
                        {
                            "name": "13:00",
                            "integration": {
                                "url": rasa_webhook_url,
                                "context": {
                                    "text": "/specify_time{\"time\" : \"13:00\"}",
                                }
                            }
                        },
                        {
                            "name": "13:30",
                            "integration": {
                                "url": rasa_webhook_url,
                                "context": {
                                    "text": "/specify_time{\"time\" : \"13:30\"}",
                                }
                            }
                        },
                        {
                            "name": "14:00",
                            "integration": {
                                "url": rasa_webhook_url,
                                "context": {
                                    "text": "/specify_time{\"time\" : \"14:00\"}",
                                }
                            }
                        },
                        {
                            "name": "14:30",
                            "integration": {
                                "url": rasa_webhook_url,
                                "context": {
                                    "text": "/specify_time{\"time\" : \"14:30\"}",
                                }
                            }
                        },
                        {
                            "name": "15:00",
                            "integration": {
                                "url": rasa_webhook_url,
                                "context": {
                                    "text": "/specify_time{\"time\" : \"15:00\"}",
                                }
                            }
                        },
                        {
                            "name": "15:30",
                            "integration": {
                                "url": rasa_webhook_url,
                                "context": {
                                    "text": "/specify_time{\"time\" : \"15:30\"}",
                                }
                            }
                        },
                        {
                            "name": "16:00",
                            "integration": {
                                "url": rasa_webhook_url,
                                "context": {
                                    "text": "/specify_time{\"time\" : \"16:00\"}",
                                }
                            }
                        },
                        {
                            "name": "16:30",
                            "integration": {
                                "url": rasa_webhook_url,
                                "context": {
                                    "text": "/specify_time{\"time\" : \"16:30\"}",
                                }
                            }
                        }, 
                        {
                            "name": "17:00",
                            "integration": {
                                "url": rasa_webhook_url,
                                "context": {
                                    "text": "/specify_time{\"time\" : \"17:00\"}",
                                }
                            }
                        },  
                        {
                            "name": "17:30",
                            "integration": {
                                "url": rasa_webhook_url,
                                "context": {
                                    "text": "/specify_time{\"time\" : \"17:30\"}",
                                }
                            }
                        }, 
                        {
                            "name": "18:00",
                            "integration": {
                                "url": rasa_webhook_url,
                                "context": {
                                    "text": "/specify_time{\"time\" : \"18:00\"}",
                                }
                            }
                        },                                                                                                
                    ]
                    }
                ]

# Zeitpunkt der nächsten Reflexion - Sammle Wochentag und Uhrzeit
class SetNewReminderForThisWeek(Action):
    def name(self) -> Text:
        return "action_set_messagetime"

    def run(self, 
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        events = []
        weekday = tracker.get_slot("weekday")
        time = tracker.get_slot("time")
        # Wenn Slots nicht befüllt sind, bitte befüllen
        if weekday==None:
            AskWeekday().run(dispatcher, tracker, domain)
            return
        elif weekday=="jetzt":
            events.append(SlotSet("weekday", None))
            events.append(SlotSet("time", None))
            time = datetime.now() + timedelta(seconds=1)
            events.append(
            ReminderScheduled(
                intent_name = 'start_reflection_session', 
                trigger_date_time = time, 
                name = "reflection_session of now", 
                kill_on_user_message= True))
            return events
        if time==None:
            AskTime().run(dispatcher, tracker, domain)
            return
        # Slots zurücksetzen
        events.append(SlotSet("weekday", None))
        events.append(SlotSet("time", None))
        time_array = str(time).split(":")
        hour1 = self.get_number(text = str(time_array[0]))
        minute1 = self.get_number(str(time_array[1]))

        weekdate1 = self.calculateNextDate(str(weekday), tracker.get_slot("number_of_session"))
        reminder_date1 = datetime(2022, weekdate1.month, weekdate1.day ,  hour1, minute1, 0, 851609)
        
        # Erinnerung an den oben bestimmten Daten setzen
        events.append(
            ReminderScheduled(
                intent_name = 'start_reflection_session', 
                trigger_date_time = reminder_date1, 
                name = "reflection_session of " + str(reminder_date1), 
                kill_on_user_message= True))
        
        # TODO Hinweis auf noch offene Reflexionssitzungen diese Woche
        text = "Erinnerung gesetzt am "+ reminder_date1.strftime("%d. %b %Y (%H:%M Uhr)")
        OutgoingWebhook.send_via_webhook(message=text, receiver=tracker.current_state()['sender_id'], attachment=self.getAttachmentAnotherDayPls() )


        return events

    def get_number(self, text: Text) -> Number:
        if text.isdigit:
            return int(text)
        else: 
            return -1
    def calculateNextDate(self, weekday: Text, number_of_session: Float) -> DateTime:
        now = datetime.now()
        weekdays = ["Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "samstag", "sonntag"]
        week_index = weekdays.index(weekday)
        now_index = now.weekday()
        if now_index < week_index:
            return now + timedelta(days = (week_index - now_index))
        else: 
            return now + timedelta(days = ( 7 + week_index - now_index))
    @staticmethod
    def getAttachmentAnotherDayPls() -> List[Dict[Text, Any]]:
        return [{
            "actions": [{
                "name": "Ich hätte doch lieber gerne einen anderen Tag.",
                "integration": {
                    "url": rasa_webhook_url,
                    "context": {
                        "text": "/choose_another_day",
                    }
                }
            }]
        }] 

# Klasse zum Versenden von Nachrichten an Mattermost mithilfe eines Webhooks 
class OutgoingWebhook(Action):
    def name(self) -> Text:
        return "outgoing_webhook"

    @staticmethod
    def send_via_webhook(message: Text, receiver: Text, attachment: List[Dict[Text, Any]]):
        payload = {
            "channel": "@" + str(receiver), 
            "text": message,
            "icon_url": "https://i.ibb.co/XXrVwYV/robot-icons-30475.png",
            "username" : "Nachhaltiges Lernen",
            "attachments": attachment
            }
        requests.post(mattermost_webhook_url, json = payload)
        return
    
    @staticmethod
    def getAttachementFinish() -> List[Dict[Text, Any]]:
        return [
                    {
                    "actions": [
                        {
                            "name": "Ich bin fertig!",
                            "integration": {
                                "url": rasa_webhook_url,
                                "context": {
                                    "text": "weiter",
                                }
                            }
                        }
                    ]
                    }
                ]
    

# Beendet eine Reflexionssitzung    
class SubmitReflectionSessionAction(Action):

    def name(self) -> Text:
        return "action_submit_reflection_session"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        username = tracker.current_state()['sender_id']
        events = []
        events.append(SlotSet("judgement", None))
        events.append(SlotSet("emotions", None))
        events.append(SlotSet("learning", None))
        events.append(SlotSet("planning", None))

        number_of_session = tracker.get_slot("number_of_session") + 1.0
        events.append(SlotSet('number_of_session', number_of_session ))

        if number_of_session == 6.0 or datetime.now() >= end_date:
            text= "Vielen Dank für die Teilnahme an dieser Studie! Wir bitten Sie zum Abschluss der Studie diese kurze Umfrage auszufüllen: https://userpage.fu-berlin.de/~besahre/survey/limesurvey/index.php/669661?lang=de"
            OutgoingWebhook.send_via_webhook(message=text, receiver=username, attachment=[] )
        else:
            text= "Vielen Dank für das Teilen Ihrer Gedanken zur Situation. Ich freue mich auf das nächste Mal! Lassen Sie uns nun den nächsten Termin planen. Es sind zwei Sitzungen pro Woche geplant. Wann möchten Sie die nächste Sitzung durchführen?"
            OutgoingWebhook.send_via_webhook(message=text, receiver=username, attachment=[] )
            AskWeekday().run(dispatcher, tracker, domain)
            # Erinnerung wenn die Antwort länger als ein Tag dauert
            events.append(ReminderScheduled(
                intent_name = 'choose_another_day', 
                trigger_date_time = datetime.now() + timedelta(days=1), 
                name = "reflection_session", 
                kill_on_user_message= True))

        return events

# Start der Studie. Ermittlung der Gruppe.
class AskForGroupAction(Action):
    def name(self) -> Text:
        return "action_ask_group_form_group"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[Dict[Text, Any]]:
        username = tracker.current_state()['sender_id']
        text = "Hallo und herzlich Willkomen zur Studie \"Nachhaltiges Lernen mit Chatbots\"!\n\n"
        text += "Zuerst ein Hinweis zum Ablauf der Studie:\n Sie werden zweimal pro Woche an einem jeweils von Ihnen gewähltem Wochentag gebeten, an einem kurzen Gespräch zum nachhaltigen Lernen teilzunehmen.\n"
        text += "Jedes Gespräch dauert etwa 7 min. Die Studie dient dazu, die Zusammenarbeit eines medizinischen Experten (das sind Sie) und eines Chatbots (das bin ich) im Bereich nachhaltiges Lernen zu erforschen. \n\n" 
        text += "Zum Start der Studie wurde Ihnen eine E-Mail mit der Gruppenzuordnung zugesandt. Welche Gruppennummer stand bei Ihrem Versuchspersonencode? Bitte klicken Sie auf den entsprechenden Button (Die E-Mail wird etwa eine Woche nach Ausfüllen des initialen Fragebogens versandt)."
        OutgoingWebhook.send_via_webhook(message=text, receiver=username, attachment=self.getAttachementGroup() )
        return []

    def getAttachementGroup(self) -> List[Dict[Text, Any]]:
        return [
                    {
                    "text": "Wählen Sie Ihre Gruppe aus.",
                    "actions": [
                        {
                            "name": "Gruppe 1",
                            "integration": {
                                "url": rasa_webhook_url,
                                "context": {
                                    "text": "1",
                                }
                            }
                        },
                        {
                            "name": "Gruppe 2",
                            "integration": {
                                "url": rasa_webhook_url,
                                "context": {
                                    "text": "2",
                                }
                            }
                        }
                    ]
                    }
                ] 

# Reflexionsaufforderung Allgemein und Spezifisch - Judgement
class AskForJudgementAction(Action):
    def name(self) -> Text:
        return "action_ask_reflection_form_judgement"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[Dict[Text, Any]]:
        if tracker.get_slot("number_of_session")>5.0:
            return
        username = tracker.current_state()['sender_id']
        events = []
        # Wähle Name je nach Session
        name= SessionInfo.getNameForSession(tracker)
        # Je nach Namen passendes Pronomen wählen
        if name == "Eberhardt":
            pronoun = "den Assistenzarzt"
        else:
            pronoun = "die Assistenzärztin"
            
        OutgoingWebhook.send_via_webhook(
            message="Sie haben sich nun in " + pronoun + " " + name + " hineinversetzt und beantworten alles aus dieser Perspektive.", 
            receiver=username, 
            attachment=[] )
        if tracker.slots.get("group")=="1":
            text = "Guten Tag Dr. " + name + ",\nbitte nehmen Sie sich sieben Minuten Zeit, um sich in die erlebte Situation zurückzuversetzen und darüber nachzudenken. Schreiben Sie alle Ihre Gedanken in den Chat. \n"
            events.append(SlotSet("judgement", ""))
            events.append(SlotSet("emotions", ""))
            events.append(SlotSet("learning", ""))
            events.append(SlotSet("planning", ""))
        else:
            pretext = "Guten Tag Dr. " + name + ", \n ich helfe Ihnen gerne, über die Situation nachzudenken. Dazu würde ich Ihnen gerne vier Fragen stellen. \n"
            pretext += "Bitte nehmen Sie sich in Summe sieben Minuten Zeit, um sich in die Situation zurückzuversetzen und die Antwort auf die Fragen in den Chat zu schreiben."
            OutgoingWebhook.send_via_webhook(message=pretext, receiver=username, attachment=[])
            text = "Wie bewerten Sie Ihre Situation (siehe oben) und Ihr Handeln in der Situation?\n"

        # Nächste Frage wenn die Antwort länger als ein Tag dauert
        events.append(ReminderScheduled(
            intent_name = 'start_reflection_session', 
            trigger_date_time = datetime.now() + timedelta(days=1), 
            name = "reflection_session", 
            kill_on_user_message= True))
        text += "Wenn Sie alle Ihre Gedanken in den Chat geschrieben haben, drücken Sie auf \"Ich bin fertig!\". Alternativ können Sie auch \"/ich-denke ich bin fertig\" in den Chat schreiben."
        OutgoingWebhook.send_via_webhook(message=text, receiver=username, attachment=OutgoingWebhook.getAttachementFinish())
        return events

# Reflexionsaufforderung Spezfifisch - Emotionen
class AskForEmotionAction(Action):
    def name(self) -> Text:
        return "action_ask_reflection_form_emotions"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[Dict[Text, Any]]:
        username = tracker.current_state()['sender_id']

        name = SessionInfo.getNameForSession(tracker)
        text = "Dr." + name + ", welche Emotionen spüren Sie, wenn Sie sich in Ihre erlebte Situation zurückversetzen?\n"
        text += "Wenn Sie alle Ihre Gedanken in den Chat geschrieben haben, drücken Sie auf \"Ich bin fertig!\""
        OutgoingWebhook.send_via_webhook(message=text, receiver=username, attachment=OutgoingWebhook.getAttachementFinish())
        # Nächste Frage wenn die Antwort länger als ein Tag dauert
        return [ReminderScheduled(
            intent_name = 'start_reflection_session', 
            trigger_date_time = datetime.now() + timedelta(days=1), 
            name = "reflection_session", 
            kill_on_user_message= True)]

# Reflexionsaufforderung Spezifisch - Lernen
class AskForLearningAction(Action):
    def name(self) -> Text:
        return "action_ask_reflection_form_learning"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[Dict[Text, Any]]:
        username = tracker.current_state()['sender_id']

        name = SessionInfo.getNameForSession(tracker)
        text = "Welche neue Erkenntnisse haben Sie, Dr. " + name + ", durch das Nachdenken über Ihre Situation erlangt?\n"
        text += "Wenn Sie alle Ihre Gedanken in den Chat geschrieben haben, bestätigen Sie mit mit Klick auf den Button."
        OutgoingWebhook.send_via_webhook(message=text, receiver=username, attachment=OutgoingWebhook.getAttachementFinish())
        # Nächste Frage wenn die Antwort länger als ein Tag dauert
        return [ReminderScheduled(
            intent_name = 'start_reflection_session', 
            trigger_date_time = datetime.now() + timedelta(days=1), 
            name = "reflection_session", 
            kill_on_user_message= True)]
# Reflexionsaufforderung Spezifisch - Planung
class AskForPlanningAction(Action):
    def name(self) -> Text:
        return "action_ask_reflection_form_planning"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[Dict[Text, Any]]:
        username = tracker.current_state()['sender_id']

        name= SessionInfo.getNameForSession(tracker)
        text = "Dr. " + name + ", wie würden Sie jetzt anders handeln, wenn Sie in eine ähnliche Situation kommen sollten?\n"
        OutgoingWebhook.send_via_webhook(message=text, receiver=username, attachment=OutgoingWebhook.getAttachementFinish())
        # Nächste Frage wenn die Antwort länger als ein Tag dauert
        return [ReminderScheduled(
            intent_name = 'start_reflection_session', 
            trigger_date_time = datetime.now() + timedelta(days=1), 
            name = "reflection_session", 
            kill_on_user_message= True)]

# Bestätigung der Teilnahme 
class ConfirmParticipationAction(Action):
    def name(self) -> Text:
        return "action_confirm_participation"
    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[Dict[Text, Any]]:
        username = tracker.current_state()['sender_id']
        group = tracker.get_slot("group")

        events = []
        reminder_date = datetime.now() + timedelta(seconds=2)
        
        # Erinnerung für jetzt
        events.append(
            ReminderScheduled(
                intent_name = 'start_reflection_session', 
                trigger_date_time = reminder_date, 
                name = "reflection_session of " + str(reminder_date), 
                kill_on_user_message = True))
        
        text = "Vielen Dank für die Teilnahme an der Studie! Sie befinden sich in Gruppe "+ group + ". Zu Beginn jeder Reflexionssitzung werden Sie von mir angeschrieben. Am Ende jeder Reflexionssitzung können Sie Wochentag und Uhrzeit für die nächste Reflexionssitzung wählen. Es sind 2 Reflexionssitzungen pro Woche geplant (insgesamt 6)."
        OutgoingWebhook.send_via_webhook(message=text, receiver=username, attachment=[])
        return events

# Start der Reflexionssitzung: Schildern des Gegenstands der Reflexion
class ReflectionThema(Action):
    def name(self) -> Text:
        return "action_reflection_thema"
    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[Dict[Text, Any]]:
        username = tracker.current_state()['sender_id']
        number_of_session = tracker.get_slot("number_of_session")
        text = self.getReflectionTheme(number_of_session)
        attachments = self.getAttachements(number_of_session)
        OutgoingWebhook.send_via_webhook(message=text, receiver=username, attachment=attachments)
        return

    def getAttachements(self, number_of_session: Float) -> List[Dict[Text, Any]]:
        if number_of_session==4.0:
            return [
                    {
                    "text": "Die von Eberhardt erhaltene Mail:",
                    "image_url": "https://i.ibb.co/bbZKgSN/2022-07-07-11-23-31-Widerherstellung-Passworts-erforderlich.png"
                    }
                ] 
        else:
            return []

    def getReflectionTheme(self, number_of_session: Float) -> Text:
        if number_of_session==0.0:
            return "Betrachten Sie folgende Situation. Versetzen Sie sich in den Assistenzarzt Eberhardt hinein und beantworten Sie anschließend die Fragen aus seiner Perspektive. \n Die elektronische Patientenakte (ePA) gibt es seit Anfang 2021. Diese soll die bessere Verfügbarkeit von Patientendaten gewährleisten. \n Der Assistenzarzt Eberhardt hat soeben das Ergebnis eines therapeutischen Gesprächs mit einer Patientin über die Ursachen ihrer Bulimia nervosa in die ePA hochgeladen.\n Dann liest er folgenden Zeitungsartikel, der ihm von einem Kollegen empfohlen wurde: \n\n\"In Finnland wurden Ende 2018 Patientendaten aus 20 Psychotherapiekliniken gestohlen. Nach erfolgloser Erpressung der Kliniken wurden tausende Patienten mit der Drohung erpresst, dass der Inhalt der therapeutischen Gespräche veröffentlicht werde, sollten die Betroffenen kein Lösegeld zahlen.\" \n\nDer Artikel lässt Eberhardt überlegen, welche Daten alle in die ePA hochgeladen werden sollten. "
        if number_of_session==1.0:
            return "Betrachten Sie folgende Situation. Versetzen Sie sich in die Assistenzärztin Vanessa hinein und beantworten Sie anschließend die Fragen aus ihrer Perspektive.\n Assistenzärztin Vanessa nimmt bei der Anamnese Daten ihres Patienten Alfred auf. Sie hat die Wahl, zu Forschungszwecken deutlich mehr Daten aufzunehmen, als eigentlich zur Behandlung des Patienten notwendig.\n Zwar ist im Kleingedruckten des zusätzlichen Anamnesebogens angegeben, dass die Angaben für den Patienten optional sind und nicht explizit für die Behandlung benötigt werden, aber Vanessa hat ihre Zweifel, ob der Patient angemessen darüber unterrichtet wurde.\n Sie beschließt, dass das nicht ihre Aufgabe ist und fährt mit der Behandlung fort."
        if number_of_session==2.0:
            return "Betrachten Sie folgende Situation. Versetzen Sie sich in die Assistenzärztin Vanessa hinein und beantworten Sie anschließend die Fragen aus ihrer Perspektive. \nAssistenzärztin Vanessa schreibt Assistenzarzt Eberhardt über WhatsApp wegen der Aufnahme von Forschungsdaten bei der Anamnese ihres Patienten. Eberhardt bestätigt ihre Bedenken und empfiehlt ein aufklärendes Gespräch bezüglich der Forschungsdaten, auch wenn das mehr Zeit beansprucht. \nAnschließend fragt Eberhardt Vanessa, ob er die psychologischen Gutachten seines Patienten in die elektronische Patientenakte hochladen sollte. Er beschreibt dabei genau, welche Inhalte im psychologischen Gutachten vorhanden sind, die er als kritisch sieht."
        if number_of_session==3.0:
            return "Betrachten Sie folgende Situation. Versetzen Sie sich in den Assistenzarzt Eberhardt hinein und beantworten Sie anschließend die Fragen aus seiner Perspektive.\nUm den Assistenzärzt:innen Zugriff auf das Computersystem von außen zu ermöglichen, wird durch die IT der Klinik ein VPN eingerichtet. \nAlle Assistenzärzt:innen können dadurch - mit Name und Passwort - von zu Hause aus auf das System und auf die enthaltenen vertraulichen Daten ihrer Patienten zugreifen. Assistenzärztin Vanessa wählt ein kompliziertes Passwort. Am nächsten Tag klagt sie darüber, dass sie ihr Passwort vergessen hat. \nDer Assistenzarzt Eberhardt kann darüber nur schmunzeln. Er wählte für seinen Account das Passwort \"12345\"."
        if number_of_session==4.0:
            return "Betrachten Sie folgende Situation. Versetzen Sie sich in den Assistenzarzt Eberhardt hinein und beantworten Sie anschließend die Fragen aus seiner Perspektive.\nDer Assistenzarzt Eberhardt bekommt eine E-Mail mit \"wiederherstellung vom ihrem password erforderlich\" im Betreff (siehe Bild unten).  \n\n Eberhardt klickt auf den in der Mail bereitgestellten Link und wird auf eine Website weitergeleitet, die genauso aussieht wie das Mattermost-System seiner Klinik. Allerdings enthält die URL oben im Browser weder das Wort Mattermost, noch ist das \"Schloss-Symbol\" sichtbar. Eberhardt gibt in die erforderlichen Felder sein Passwort ein und klickt auf \"Passwort wiederherstellen\". \nZwei Wochen später folgt eine Rundmail der IT, dass es einen Hacker-Angriff gegeben hat, bei dem Daten gestohlen wurden. Eberhardt denkt an die stattgefundene Situation zurück."
        if number_of_session==5.0:
            return "Betrachten Sie folgende Situation. Versetzen Sie sich in die Assistenzärztin Vanessa hinein und beantworten Sie anschließend die Fragen aus ihrer Perspektive.\nDie Assistenzärztin Vanessa meldet sich über das VPN in ihrer Klinik an, um sich ihren Terminkalender anzuschauen – der Chefarzt sprach mit ihr am Nachmittag über einen Termin, der angepasst wurde. \nBei der Anmeldung im VPN fällt ihr auf, dass sie sich auch ohne Passwort, nur mit Angabe ihres Benutzernamens, anmelden kann. Vanessa freut sich, weil sie sowieso ihr Passwort vergessen hatte. Vanessa weiß, dass sie morgen noch mehrfach über das VPN auf das System zugreifen muss, daher beschließt sie, die fehlende Passwortabfrage erst in zwei Tagen der IT zu melden."
        return "Sie haben bereits alle Szenarien erlebt. Vielen Dank für die Teilnahme an dieser Studie! Wir bitten Sie zum Abschluss der Studie diese kurze Umfrage auszufüllen: https://userpage.fu-berlin.de/~besahre/survey/limesurvey/index.php/669661?lang=de"


# Hilfsklasse zur Bestimmung der Pronomen für die Reflexionstexte
class SessionInfo(Action):
    def name(self) -> Text:
        return "action_session_info"
    @staticmethod
    def getNameForSession(tracker: Tracker) -> Text:
        number_of_session = tracker.slots.get("number_of_session")
        if number_of_session==0.0 or number_of_session==3.0 or number_of_session==4.0:
            return "Eberhardt"
        else:
            return "Vanessa"


